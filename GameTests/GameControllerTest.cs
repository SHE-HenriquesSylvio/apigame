using System;
using System.Collections.Generic;
using AutoMapper;
using APIGame.Controllers;
using APIGame.Dtos;
using APIGame.Profiles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Game.Tests
{
    [TestClass]
    public class GameControllerTest
    {
        private MapperConfiguration _configuration;
        private IMapper _mapper;
        private GameController controller;
        public GameControllerTest()
        {
            _configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new GamesProfile());
            });
            _mapper = new Mapper(_configuration);
            this.controller = new GameController(_mapper);
        }

        [TestMethod]
        public void GetGames()
        {
            var result = this.controller.GetGames();
            Assert.IsInstanceOfType(result, typeof(ActionResult<IEnumerable<GameReadDto>>));
        }

        [TestMethod]
        public void GetGamedById_Id_true()
        {
            var result = this.controller.GetGamedById(2);
            Assert.IsInstanceOfType(result.Result, typeof(Microsoft.AspNetCore.Mvc.OkObjectResult));
        }

        [TestMethod]
        public void GetGamedById_Id_false()
        {
            var result = this.controller.GetGamedById(97);
            Assert.IsInstanceOfType(result.Result, typeof(Microsoft.AspNetCore.Mvc.NotFoundResult));
        }

        [TestMethod]
        public void GetGamedById_Type_true()
        {
            var result = this.controller.GetGamedById(2);
            Assert.IsInstanceOfType(result, typeof(ActionResult<GameReadDto>));
        }

        [TestMethod]
        public void CreateGame_notnull()
        {
            var result = this.controller.CreateGame(new GameCreateDto { ID = 10, Genre = "Genre", Name = "Name" });
            Assert.IsInstanceOfType(result, typeof(ActionResult<GameReadDto>));
        }

        [TestMethod]
        public void CreateGame_null()
        {
            Assert.ThrowsException<ArgumentNullException>((() => this.controller.CreateGame(null)));
        }

        [TestMethod]
        public void UpdateGame_Id_true()
        {
            var result = this.controller.UpdateGame(2, new GameUpdateDto { Genre = "Genre", Name= "Name" });
            Assert.IsInstanceOfType(result, typeof(Microsoft.AspNetCore.Mvc.NoContentResult));
        }

        [TestMethod]
        public void UpdateGame_Id_false()
        {
            var result = this.controller.UpdateGame(97, new GameUpdateDto());
            Assert.IsInstanceOfType(result, typeof(Microsoft.AspNetCore.Mvc.NoContentResult));
        }

        [TestMethod]
        public void DeleteGame_Id_true()
        {
            var result = this.controller.DeleteGame(3);
            Assert.IsInstanceOfType(result, typeof(Microsoft.AspNetCore.Mvc.NoContentResult));
        }

        [TestMethod]
        public void DeleteGame_Id_false()
        {
            var result = this.controller.DeleteGame(97);
            Assert.IsInstanceOfType(result, typeof(Microsoft.AspNetCore.Mvc.NoContentResult));
        }
    }
}