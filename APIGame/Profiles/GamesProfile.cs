﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using APIGame.Dtos;
using APIGame.Models;

namespace APIGame.Profiles
{
    public class GamesProfile : Profile
    {
        public GamesProfile()
        {
            CreateMap<GameCreateDto, Game>();
            CreateMap<Game, GameReadDto>();
            CreateMap<GameUpdateDto, Game>();
            CreateMap<Game, GameUpdateDto>();
        }

    }
}
