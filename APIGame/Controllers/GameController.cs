﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using APIGame.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using AutoMapper;
using APIGame.Dtos;
using Microsoft.AspNetCore.Http;

namespace APIGame.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private List<Game> Games;
        private readonly IMapper _mapper;
        private const string _repo = "./data.json";

        public GameController(IMapper mapper)
        {
            _mapper = mapper;
            string ListeGame = System.IO.File.ReadAllText(_repo);
            Games = JsonConvert.DeserializeObject<IEnumerable<Game>>(ListeGame).ToList();
        }

        public void SaveChanges()
        {
            System.IO.File.Delete(_repo);
            System.IO.File.AppendAllText(_repo, JsonConvert.SerializeObject(Games));
        }

        /// <summary>
        /// Retourne la liste de tout les jeux 
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Retourne la listes des jeux</response>
        [HttpGet]
        public ActionResult<IEnumerable<GameReadDto>> GetGames()
        {
            return Ok(_mapper.Map<IEnumerable<GameReadDto>>(Games));
        }

        /// <summary>
        /// Retourne la guild avec l'id correspondant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Retourne la listes des jeux</response>
        /// <response code="404">Si aucun jeu correspondant à l'id</response>
        [HttpGet("{id}")]
        public ActionResult<GameReadDto> GetGamedById(int id)
        {
            var game = Games.FirstOrDefault(g => g.ID == id);
            if (game != null) return Ok(_mapper.Map<GameReadDto>(game));
            return NotFound();
        }

        /// <summary>
        /// Permet de créer un nouveau jeu
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        /// <response code="200">Créer le jeu renseigner</response>
        [HttpPost]
        public ActionResult<GameReadDto> CreateGame(GameCreateDto game)
        {
            if (game == null)
            {
                throw new ArgumentNullException();
            }
            var gameModel = _mapper.Map<Game>(game);
            Games.Add(gameModel);
            SaveChanges();
            return Ok(_mapper.Map<GameReadDto>(gameModel));
        }

        /// <summary>
        /// Permet de modifier les informations d'un jeu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gameDto"></param>
        /// <returns></returns>
        /// <response code="204">Retourne les modifications du jeu</response>
        /// <response code="404">Si aucun jeu correspondant à l'id</response>
        [HttpPut("{id}")]
        public ActionResult UpdateGame(int id, GameUpdateDto gameDto)
        {
            var game = Games.FirstOrDefault(i => i.ID == id);
            _mapper.Map(gameDto, game);
            SaveChanges();
            return NoContent();
        }

        /// <summary>
        /// Permet de supprimer un jeu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">Supprime le jeu</response>
        /// <response code="404">Si aucun jeu correspondant à l'id</response>
        [HttpDelete("{id}")]
        public ActionResult DeleteGame(int id)
        {
            var game = Games.FirstOrDefault(i => i.ID == id);
            Games.Remove(game);
            SaveChanges();
            return NoContent();
        }
    }
}
