﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIGame.Dtos
{
    public class GameUpdateDto
    {
        public string Name { get; set; }
        public string Genre { get; set; }
    }
}
